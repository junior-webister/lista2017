<?php 
	include 'seguranca.php'; 
  include 'model.php';
	protegePagina();
	$usuario = $_SESSION['usuarioNome'];
?>
<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="materialize/dist/css/materialize.min.css"  media="screen,projection"/>
      <link rel="icon" href="_imagens/icone.png" type="image/x-icon"/>
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>LISTA PRESENTES | 2017</title>
    </head>

    <body>
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="materialize/dist/js/materialize.min.js"></script>
      <nav>
    	<div class="nav-wrapper green darken-2">
      		<a href="#!" class="brand-logo">&nbsp;Amigo Secreto</a>
      		<ul class="right hide-on-med-and-down">
      			<li><?php echo "Bem-vindo, " . $usuario . " &nbsp;&nbsp;&nbsp;"; ?></li>
        		<li id="home"><a href="index.php">Lista de Presentes</a></li>
        		<li id="cadastro"><a href="cadastro.php">Cadastro</a></li>
        		<!-- <li><a href="badges.html"><i class="material-icons right">view_module</i>Link with Right Icon</a></li> -->
        		<li><a href="logout.php"><i class="material-icons left">power_settings_new</i>Sair</a></li>
      		</ul>
    	</div>
  </nav>
    </body>
  </html>
