<?php 
	include 'cabecalho.php';

 ?>

 <div class="container section">
	<h5 class="center">Cadastro <p class="flow-text">Cadastre os seus presentes aqui.</p></h5>
	<div class="divider"></div>
	<div class="container">
	<div class="row">
    <form class="col s12" method="post" action="inserirPresentes.php">
      <div class="row">
        <div class="input-field col s12">
          <textarea id="textarea1" name="op1" class="materialize-textarea"></textarea>
          <label for="textarea1">1a Opção</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <textarea id="textarea2" name="op2" class="materialize-textarea"></textarea>
          <label for="textarea2">2a Opção</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <textarea id="textarea3" name="op3" class="materialize-textarea"></textarea>
          <label for="textarea3">3a Opção</label>
        </div>
      </div>
      <input class="right red darken-2 btn" type="submit" name="cadastrar" value="Cadastrar">
    </form>
  </div>
  </div>
</div>

 <script type="text/javascript">
 	$(document).ready(function(){
 		$('#cadastro').addClass('active');
 		//alert('ola');
 	});
 </script>