<?php 
	include 'cabecalho.php';
	$user = $_SESSION['usuarioNome'];
	$id_user = $_SESSION['usuarioID'];
?>

<div class="container section">
	<h5 class="center"><i class="material-icons">format_list_numbered</i> Lista de Presentes </h5>
	<div class="divider"></div>
	<br>
	<table id="tblEditavel" class="bordered striped">
		<thead>
			<th>Nome</th>
			<th>Opção 1</th>
			<th>Opção 2</th>
			<th>Opção 3</th>
			<th>#</th>
		</thead>
	<?php 
		$buscaLista = mysqli_query($_SG['link'], "SELECT a.*,b.nome_pessoa FROM opcoes a inner join usuarios b on a.id_usuario = b.id");

		while ($lista = mysqli_fetch_array($buscaLista)) {
			echo "<tr>";
			//echo "<td>". $lista['id'] ."</td>";
			echo "<td>". $lista['nome_pessoa'] ."</td>";

			if ($id_user == $lista['id_usuario']) {
				echo "<td title='opcao1' class='editavel'>". $lista['opcao1'] ."</td>";
				echo "<td title='opcao2' class='editavel'>". $lista['opcao2'] ."</td>";
				echo "<td title='opcao3' class='editavel'>". $lista['opcao3'] ."</td>";
				//echo "<td><a class='btn-floating red' href='deletaPresentes.php?id=".$lista['id']."'><i class='material-icons'>delete</i></a></td>";
				echo "<td><a id='modalBtn' class='btn-floating red' href='#modal1'><i class='material-icons'>delete</i></a></td>";
				echo '   <!-- Modal Structure -->
						  <div id="modal1" class="modal">
						    <div class="modal-content">
						      <h4>Deletar</h4>
						      <p>Deseja realmente deletar seu pedido?</p>
						    </div>
						    <div class="modal-footer">
					    		<a href="javascript:location.reload();" class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
						      <a href="deletaPresentes.php?id='.$lista['id'].'" class="modal-action modal-close waves-effect waves-red btn-flat">Deletar</a>
						    </div>
						  </div>';
			} else {
				echo "<td>". $lista['opcao1'] ."</td>";
				echo "<td>". $lista['opcao2'] ."</td>";
				echo "<td>". $lista['opcao3'] ."</td>";
				echo "<td><a class='btn-floating red disabled' href='#'><i class='material-icons'>delete</i></a></td>";
			}
			echo "</tr>";
		}


	 ?>
 	</table>
</div>

<script type="text/javascript">
 	$(document).ready(function(){
 		$('#home').addClass('active');
 	});

 	$(document).ready(function() {
  $('#tblEditavel tbody tr td.editavel').dblclick(function() {
      if ($('td > input').length > 0) {
        return;
      }
      var conteudoOriginal = $(this).text();
      var novoElemento = $('<input/>', {
        type: 'text',
        value: conteudoOriginal
      });
      $(this).html(novoElemento.bind('blur keydown', function(e) {
        var keyCode = e.which;
        var conteudoNovo = $(this).val();
        if (keyCode == 13 && conteudoNovo != '' && conteudoNovo != conteudoOriginal) {
          var objeto = $(this);
          $.ajax({
            type: "POST",
            url: "alteraPresentes.php",
            data: {
              id: $(this).parents('tr').children().first().text(),
              campo: $(this).parent().attr('title'),
              valor: conteudoNovo
            }, //added this comma here
            success: function(result) {
              objeto.parent().html(conteudoNovo);
              $('body').append(result);
            }
          })
        } else if (keyCode == 27 || e.type == 'blur'){
          $(this).parent().html(conteudoOriginal);
          }
      }));
      $(this).children().select();
    
        //} removed the extra } from here.
    });

  })


  $(document).ready(function(){
    // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
    $('#modalBtn').click(function(){
    	$('.modal').show();
    });
  });
          
 </script>

